import { MikroORM } from "@mikro-orm/core";
import { __dev__ } from "./constants";
import path from 'path';
import User from "./entities/User";
import Post from "./entities/Post";

export const microConfig: Parameters<typeof MikroORM.init>[0] = {
  migrations: {
    path: path.join(__dirname, './migrations'), // path to the folder with migrations and that "join" create absoulte path
    pattern: /^[\w-]+\d+\.[tj]s$/, // regex pattern for the migration files means only accept typescript and javascrip
  },
  entities: [Post, User],
  dbName: "test",
  user: "postgres",
  password: "sina2581343",  
  type: "postgresql",
  debug: __dev__
} ; 

export default microConfig;