import "reflect-metadata";
import { ApolloServer } from 'apollo-server-express';
import { MikroORM } from "@mikro-orm/core";
import { __dev__, __prod__ } from './constants';
import microConfig from "./mikro-orm.config";
import express from "express";
import { buildSchema } from 'type-graphql'
import { PostResolver } from './resolvers/post';
import { UserResolver } from "./resolvers/user";
import redis from 'redis';
import session from 'express-session';
import connectRedis from 'connect-redis';
import { MyContext } from "./types";

const main = async () => {
  const orm = await MikroORM.init(microConfig);
  await orm.getMigrator().up();
  
  const app = express();


  const RedisStore = connectRedis(session)
  const redisClient = redis.createClient()

  app.use(
    session({
      name: 'qid',
      store: new RedisStore({
        client: redisClient,
        disableTouch: true
      }),
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 365, // one years
        httpOnly: true,
        sameSite: "lax", // scrf reasons
        secure: __prod__ // if we set it true only works in https
      },
      saveUninitialized: false,
      secret: 'fkldkldsjfioswekuihfdshgfh',
      resave: false,
    })
  )

  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [PostResolver, UserResolver],
      validate: false
    }),
    context: ({req, res}): MyContext => ({ em: orm.em, req, res })
  });
  
  apolloServer.applyMiddleware({ app });// create a graphql end-point

  app.listen({port: 4000},()=>{
    console.log(`Server ready at http://localhost:4000${apolloServer.graphqlPath}`);
  });
}

main();