import { Entity, PrimaryKey, Property } from "@mikro-orm/core";
import { Field, ObjectType, Int, GraphQLISODateTime } from "type-graphql";

@ObjectType()
@Entity()
export class Post {
  @Field(() => Int)
  @PrimaryKey()
  id!: number;

  @Field(() => GraphQLISODateTime)
  @Property({type: 'date'})
  createdAt = new Date();

  @Field(() => GraphQLISODateTime)
  @Property({ type: "date", onUpdate: () => new Date() })
  updatedAt = new Date();

  @Field(() => String)
  @Property({type: "text"}) // these are represent of colomuns in db
  title!: string;
}

export default Post;