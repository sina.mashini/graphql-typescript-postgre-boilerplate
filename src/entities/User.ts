import { Entity, PrimaryKey, Property } from "@mikro-orm/core";
import { Field, ObjectType, Int, GraphQLISODateTime } from "type-graphql";

@ObjectType()
@Entity()
export class User {
  @Field(() => Int)
  @PrimaryKey()
  id!: number;

  @Field(() => GraphQLISODateTime)
  @Property({type: 'date'})
  createdAt = new Date();

  @Field(() => GraphQLISODateTime)
  @Property({ type: "date", onUpdate: () => new Date() })
  updatedAt = new Date();

  @Field(() => String)
  @Property({type: "text"}) 
  fullname!: string;

  @Field(() => String)
  @Property({type: "text", unique: true}) 
  email!: string;
  
  @Property({type: "text"}) // it's not gonna show in graphql pg
  password!: string;
}

export default User;