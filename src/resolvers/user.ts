import User from "../entities/User";
import { MyContext } from "src/types";
import { Arg, Ctx, Field, InputType, Mutation, ObjectType, Query, Resolver } from "type-graphql";
import argon2 from 'argon2'

@InputType()
class UserRegisterInput {
  @Field()
  email: string;
  @Field()
  password: string;
  @Field()
  confirmPass: string;
  @Field()
  fullname: string;
}

@InputType()
class UserNamePassWordInput {
  @Field()
  email: string;
  @Field()
  password: string;
}

@ObjectType() 
class FieldError {
  @Field()
  field: string;
  @Field()
  message: string
} 

@ObjectType()
class UserResponse {
  @Field(()=> [FieldError], {nullable: true})
  errors?: FieldError[]

  @Field(()=> User, {nullable: true})
  user?: User
}

@Resolver()
export class UserResolver {
  @Mutation(() => UserResponse)
  async register(
    @Arg('options') options: UserRegisterInput,
    @Ctx() {em, req}: MyContext
  ) {
   
      if (!options.email || !options.fullname || !options.password) {
        return {
          errors: [{
            field: "everything",
            message: "please enter everything"
          }]
        }
      }
      if (options.confirmPass != options.password)
        return {
          errors: [{
            field: "password & confim password",
            message: "password and confirm password are not equal"
          }]
        }
    const hashedPassword = await argon2.hash(options.password);
    const user = em.create(User, { email: options.email, fullname: options.fullname, password: hashedPassword });
    try {
      await em.persistAndFlush(user);
    } catch (err) {
      return {
        errors: [{
          // you can check error codes from here https://www.postgresql.org/docs/10/errcodes-appendix.html#:~:text=All%20messages%20emitted%20by%20the,at%20the%20textual%20error%20message.
          message: (err.code == 23505)? "User already exists" : "Something went wrong!"
        }]
      }
    }  
    // login user after registration
    req.session!.userId = user.id;

    return { user };
  }

  @Mutation(() => UserResponse)
  async login(
    @Arg('options') options: UserNamePassWordInput,
    @Ctx() {em, req}: MyContext
  ): Promise<UserResponse> {

    const user = await em.findOne(User, { email: options.email.toLowerCase() });
    if (!user) {
      return {
        errors: [
          {
            field: "email",
            message: "the user dosen't exist"
          }
        ]
      }
    }
    const validPass = await argon2.verify(user.password, options.password);
    if (!validPass) {
      return {
        errors: [
          {
            field: "email | password",
            message: "email or password is wrong"
          }
        ]
      }
    }

    req.session!.userId = user.id;

    return {
      user
    }
  }
  
  @Query(() => User, { nullable: true })
  me(
    @Ctx() {req, em}: MyContext
  ): Promise<User | null> | null {
    if (!req.session!.userId) return null;
    const user = em.findOne(User, { id: req.session!.userId })
    return user;
  }

  @Query(() => [User], { nullable: true })
  users(
    @Ctx() {em}: MyContext
  ): Promise<User[] | null> { 
    return em.find(User, {});
  }
}